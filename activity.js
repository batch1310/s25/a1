// Count the total number of Fruits on Sale
db.fruits.aggregate([
	{ $match: { onSale: true} },
	{ $count: "fruitsOnSale"}
]);

// Count the total number of Fruits with stock more than 20
db.fruits.aggregate([
	{ $match: { stock: {$gte: 20}} },
	{ $count: "enoughStock"}
]);

// Get the average price of the fruits onSale per Supplier
db.fruits.aggregate([
	{ $match: { onSale: true} },
	{ $group: { _id: "$supplier_id", avg_price: { $avg: "$price"}} },
	{ $sort: { total: 1} }
]);

// max operator - highest price of a fruit per supplier
db.fruits.aggregate([
	{ $match: { onSale: true} },
	{ $group: { _id: "$supplier_id", max_price: { $max: "$price"}} }
]);

// min operator
db.fruits.aggregate([
	{ $match: { onSale: true} },
	{ $group: { _id: "$supplier_id", min_price: { $min: "$price"}} }
]);
